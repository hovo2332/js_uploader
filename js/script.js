const inpFile = document.getElementById('inpFile');
const uploadBtn = document.getElementById('uploadBtn');
const uploadForm = document.getElementById('uploadForm');
const progressBarText = document.querySelector('.progress-bar-text');
let progressBar = document.getElementById('progressBar');
let container = document.querySelector('.container');
const url = "http://192.168.0.107/";




function uploadFile(x, delta, files) {
  for (let i = x; i < x + delta; i++) {

    if (!files[i]) {
      return
    }

    const xhr = new XMLHttpRequest();
    const formData = new FormData();

    formData.append("files", files[i]);




    xhr.open("POST", `${url}`);


    xhr.upload.addEventListener("progress", e => {

      const percent = e.lengthComputable ? (e.loaded / e.total) * 100 : 0;

      document.querySelector(`div[data-name="${files[i].name}"]`).style.width = percent.toFixed(2) + "%";
      document.querySelector(`span[data-name="${files[i].name}"]`).textContent = percent.toFixed(2) + "%";

    });

    xhr.send(formData);

    xhr.onreadystatechange = function () {

      if (xhr.readyState == 4 && JSON.parse(xhr.response).status) {

        x++;


        if (x % delta == 0) {
          uploadFile(x, delta, files);
        }
      }
    }

  }
}



function loadBar(files) {

  for (let i = 0; i < files.length; i++) {

    let type = files[i].type.split('/').shift();

    container.innerHTML +=
      `
    <div class='icon'>
    <p class="fileName">${files[i].name}</p>
    ${type =="image" ? `<img src="" data-name="${files[i].name}" class="icon__img">` : `<i class="fas fa-file"></i>`}
    <div>
    <div class="progress-bar" id="progressBar">
    <div class="progress-bar-fill" data-name="${files[i].name}">
    <span class="progress-bar-text" data-name="${files[i].name}">0%</span>
    </div>
    </div>
    
    `;


    if (type == "image") {

      let reader = new FileReader();

      reader.onload = function (e) {
        let src = e.target.result;
        document.querySelector(`img[data-name="${files[i].name}"]`).src = src;
      }

      reader.readAsDataURL(files[i]);
    }
  }

}




function dropHandler(ev) {

  ev.preventDefault();

  if (ev.dataTransfer.items) {
    let arr = [];

    for (let i = 0; i < ev.dataTransfer.items.length; i++) {
      if (ev.dataTransfer.items[i].kind === 'file') {
        let files = ev.dataTransfer.items[i].getAsFile();
        arr.push(files)
      }
    }

    container.innerHTML = "";
    loadBar(arr);

    uploadBtn.addEventListener("click", dragParams);
    uploadBtn.param = arr;
  }
}

function dragOverHandler(ev) {
  ev.preventDefault();
}


function dragParams(e) {
  let x = 0;
  let delta = 3;
  uploadFile(x, delta, e.currentTarget.param);

}

inpFile.addEventListener('change', function () {
  container.innerHTML = "";
  let files = inpFile.files;
  loadBar(files);
  uploadBtn.addEventListener("click", dragParams,{once:true});
  uploadBtn.param = files;
});